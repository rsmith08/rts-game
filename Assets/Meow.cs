﻿using UnityEngine;
using System.Collections;


public class Meow : MonoBehaviour {
	//needed for everything
	public int teamNumber;
	public int AllianceNumber;
	public int AttackDistance;
	public Vector3 CurrentPos;
	public Vector2 ScreenPos;
	public bool OnScreen;
	public bool Selected = false;
	public bool isWalkable = true;
	private Vector3 destination;
	private Vector2 barPos;
	public bool beingDamaged = false;
	public bool canAttack = false;
	public GameObject ChosenGameObject;


	//needed for health stuff
	public bool isShieldActive = true;
	
	public float curHealth ;
	public float maxHealth;
	
	public float curShield;
	public float maxShield;
	
	public float barLength;
	public float BarPercentage;
	public Texture2D healthBar;
	public Texture2D shieldBar;
	public Texture2D barBackground;
	public float HpBarLength;
	public float HpPercentage;
	
	public float ShieldBarLength;
	public float ShieldBarPercentage;
	private float xoffset = -0.5f;
	private float yoffset = 1f;

	void Start () {
		curHealth = maxHealth;
		curShield = maxShield;
	}
	public void LateUpdate(){
		GameObject ThisOBJ = this.GetComponent<Collider>().gameObject;
		
		
		if(curHealth <= 0){
			if(Selected){
				MouseControl.RemoveUnitFromCurrentlySelectedUnits(ThisOBJ);
			}
			MouseControl.RemoveFromOnScreenUnits(ThisOBJ);
			
			
			
			Destroy(this.gameObject);
			
			
		}
		destination = Vector3.zero;
		
		
		if (beingDamaged == true) {
			if(isShieldActive){
				curShield -= 20;
				
				
				
				
			}
			else if(!isShieldActive){
				curHealth -= 40;
				
			}
			
			
			
		}
		
		if (curShield <= 0) {
			isShieldActive = false;		
			
		}
		if (curShield < maxShield) {
			curShield += (10 * Time.deltaTime);		
			
			
		} else if (curShield > maxShield) {
			
			curShield = maxShield;
		}
	}
	// Update is called once per frame
	void OnGui(){
		if (Selected) {
			//bar background
			GUI.DrawTexture(new Rect(barPos.x -5,barPos.y,barLength,10),barBackground);
			GUI.DrawTexture(new Rect(barPos.x - 5,barPos.y+15,barLength,10),barBackground);
			
			//actual bars
			
			GUI.DrawTexture (new Rect (barPos.x, barPos.y, HpBarLength, 10), healthBar);
			GUI.DrawTexture (new Rect (barPos.x, barPos.y + 15, ShieldBarLength, 10), shieldBar);
		}


	}
	void Update () {

		
		barPos = Camera.main.WorldToScreenPoint (new Vector3(this.transform.position.x + xoffset,this.transform.position.y + yoffset,this.transform.position.z));
		barPos.y = Screen.height - barPos.y;
		if (Selected) {
			//draw health bar....		
			curHealth = Mathf.Clamp(curHealth, 0, maxHealth);
			curShield = Mathf.Clamp(curShield, 0, maxShield);
			
			BarPercentage = maxShield / maxShield;
			barLength = BarPercentage * 100 + 10;
			
			HpPercentage = curHealth / maxHealth;
			HpBarLength = HpPercentage * 100 ;
			
			ShieldBarPercentage = curShield / maxShield;
			ShieldBarLength = ShieldBarPercentage * 100;
			
			if(isWalkable){
				
			}
			
			
		}
				GameObject ThisOBJ = this.GetComponent<Collider>().gameObject;
		CurrentPos = this.transform.position;
		
		//if unit not selected, get screen space
		if (!Selected) {
			
			ScreenPos = Camera.main.WorldToScreenPoint(CurrentPos);
			
			if(MouseControl.UnitWithinScreenSpace(ScreenPos)){
				if(!MouseControl.UnitAlreadyOnScreen(ThisOBJ)){
					
					
					MouseControl.UnitsOnScreen.Add(ThisOBJ);
				}
			}
			if(!MouseControl.UnitWithinScreenSpace(ScreenPos)){
				MouseControl.RemoveFromOnScreenUnits(ThisOBJ);
				
				
			}
			
			if(!MouseControl.UnitInsideDrag(ScreenPos)){
				ThisOBJ.transform.FindChild ("Selected").gameObject.SetActive(false);
				ThisOBJ.GetComponent<Unit> ().Selected = false;
				MouseControl.RemoveUnitFromCurrentlySelectedUnits(ThisOBJ);
				
			}
			
			//get screen space of unit, via world to screen point
			//then check if it is on the screen, if so add to on screen Array list...
			
			//if in drag add to dragged units, if only single selection with out shift, clear selected units and add single 
			
			
			
		}
		
		
		
	}
	public void UnitMoveCheck(){
		GameObject ThisOBJ = this.GetComponent<Collider>().gameObject as GameObject;
		if(
			ThisOBJ.transform.FindChild("Unit") == true){
			//this is a unit so it can move
			//so take in the right click point 		if (Item.Selected == true && !MouseControl.UserIsDragging) {
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			/**if(Input.GetMouseButtonUp(1) && !MouseControl.AltHasBeenPressed() && !MouseControl.UserIsDragging  ){
				destination = MouseControl.moveToPoint;
				
				ThisOBJ.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(destination);
			}
			
			**/
			
			
			
			
		}



		}



	}


