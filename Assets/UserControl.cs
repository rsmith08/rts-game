﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;




public class UserControl : MonoBehaviour {
	public static float ScrollSpeed = 100;//default speed
	public static float RotateSpeed = 100;//default speed
	public static float RotateAmount = 10;
	public static int ScrollWidth = 30;
	public static float MinCameraHeight = 10;
	public static float MaxCameraHeight = 200;
	public static float ScrollSpeedUp;


	// Use this for initialization
	void Start () {





	}

	// Update is called once per frame
	void Update () {


		if (Input.GetKey (KeyCode.LeftShift) || Input.GetKey (KeyCode.RightShift)) {//this logic checks if the shift keys are pressed, this is to increase  the speed at which the camera moves
			ScrollSpeedUp = ScrollSpeed * 2;	


		} 
		else {

			ScrollSpeedUp = ScrollSpeed;

		}

		if (!Input.GetKey (KeyCode.LeftAlt) ) //left alt is used for rotation
		{
			MoveCamera ();//calls function
		}
		else{
			RotateCamera();//calls function
		}









	}

	private void MoveCamera(){
		float xpos = Input.mousePosition.x;//gets mouse position
		float ypos = Input.mousePosition.y;//gets mouse position
		Vector3 movement = new Vector3 (0, 0, 0);//creates an empty vector





		bool mouseScroll = false;

		//horizontal camera movement
		if(xpos <= 0 && xpos <  ScrollWidth || Input.GetKey(KeyCode.A)) {//checks to make sure xpos is less than 0 as screen space cannot be negative
			movement.x -=  ScrollSpeedUp;

			mouseScroll = true;
		} else if(xpos <= Screen.width && xpos > Screen.width -  ScrollWidth || Input.GetKey (KeyCode.D)) {
			movement.x +=  ScrollSpeedUp;

			mouseScroll = true;
		}

		//vertical camera movement
		if(ypos >= 0 && ypos <  ScrollWidth || Input.GetKey( KeyCode.S)) {
			movement.z -=  ScrollSpeedUp;

			mouseScroll = true;
		} else if(ypos <= Screen.height && ypos > Screen.height -  ScrollWidth || Input.GetKey (KeyCode.W) ){
			movement.z +=  ScrollSpeedUp;

			mouseScroll = true;
		}




		movement = Camera.main.transform.TransformDirection (movement);
		movement.y = 0;


		//Y movement scroll wheel!
		movement.y -=  ScrollSpeed * Input.GetAxis("Mouse ScrollWheel");




		// calculates movement!
		Vector3 origin = Camera.main.transform.position;
		Vector3 destination = origin;
		destination.x += movement.x;
		destination.y += movement.y;
		destination.z += movement.z;



		if (destination.y >  MaxCameraHeight) {
			destination.y =  MaxCameraHeight;	
		} else if (destination.y <  MinCameraHeight) {
			destination.y =  MinCameraHeight;
		}


		// if change is made perform update!
		if (destination != origin) {
			Camera.main.transform.position = Vector3.MoveTowards(origin, destination, Time.deltaTime *  ScrollSpeed);
		}




	}



	private void RotateCamera(){
		Vector3 origin = Camera.main.transform.eulerAngles;
		Vector3 destination = origin;

		//detect rotation using alt key and right mouse
		if ((Input.GetKey (KeyCode.LeftAlt) || Input.GetKey (KeyCode.RightAlt)) && Input.GetMouseButton (1)) {
			destination.x -= Input.GetAxis("Mouse Y") *  RotateAmount;
			destination.y += Input.GetAxis("Mouse X") *  RotateAmount;
		}

		//update if changed !

		if (destination != origin) {
			Camera.main.transform.eulerAngles = Vector3.MoveTowards(origin, destination, Time.deltaTime *  RotateSpeed);}

	}





}