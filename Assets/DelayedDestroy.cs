﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedDestroy : MonoBehaviour {
	public float timeUntilDestroy = 10f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (timeUntilDestroy >= 0) {
			timeUntilDestroy -= Time.deltaTime;
			Debug.Log ("Time Until Destroy = "+timeUntilDestroy);
		} else {
			Destroy (this.gameObject);
		}
	}
}
